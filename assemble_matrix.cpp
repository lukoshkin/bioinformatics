#include "DM.h"

void DM::assemble_matrix () 
{ 
    for (int i=0; i<N+1; i++) 
        matrix[i][0] = i; 

    for (int i=1; i<M+1; i++) 
        matrix[0][i] = i; 

    for (int i=1; i<N+1; i++) 
    { 
        for (int j=1; j<M+1; j++) 
        { 
            int a = matrix[i][j-1] + 1; 
            int b = matrix[i-1][j] + 1; 
            int c = matrix[i-1][j-1] + ( sq1[i-1] != sq2[j-1] ); 
            matrix[i][j] = std::min(a,std::min(b,c)); 
        } 
    } 
}
