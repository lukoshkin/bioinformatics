#ifndef __DM__
#define __DM__

#include <string>

using str=std::string;

class DM                                                                        
{                                                                               
    str sq1, sq2;                                                               
    int N, M;                                                                   
    int ** matrix;                                                              
                                                                                
    public:                                                                     
        DM (str,str);                                                           
        ~DM ();                                                                 
        void assemble_matrix();                                                 
        int get_last();                                                         
        void print_matrix ();                                                   
};

#endif
