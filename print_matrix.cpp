#include <iostream>
#include "DM.h"

void DM::print_matrix ()                                                        
{                                                                               
    std::cout << " \t \t";                                                      
    for (int i=0; i<M; i++)                                                     
        std::cout << sq2[i] << '\t';                                            
    std::cout << sq2[M] << '\n';                                                
                                                                                
    std::cout << " \t";                                                         
    for (int i=0; i<M; i++)                                                     
        std::cout << matrix[0][i] << '\t';                                      
    std::cout << matrix[0][M] << '\n';                                          
                                                                                
    for (int i=1; i<N+1; i++)                                                   
    {                                                                           
        std::cout << sq1[i-1] << '\t';                                          
        for (int j=0; j<M; j++)                                                 
            std::cout << matrix[i][j] << '\t';                                  
        std::cout << matrix[i][M] << '\n';                                      
    }                                                                           
}                 
