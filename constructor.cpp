#include "DM.h"

DM::DM (str word1, str word2)                                                    
{                                                                                
    sq1 = word1, sq2 = word2;                                                    
    N = sq1.size(), M = sq2.size();                                              
                                                                                 
    matrix = new int * [N+1];                                                    
    for (int i=0; i<N+1; i++)                                                    
        matrix[i] = new int [M+1];                                               
}             
