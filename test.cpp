#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <locale>
#include <algorithm>
#include "DM.h"

using str = std::string;
using Map = std::map<str,str>;

void print_dict (Map dict)
{
    for (auto it : dict)
    {
            std::cout << it.first << " : " << it.second << '\n';
    }
}

int Lev_Dist(str key1, str key2, Map dict)
{
    DM tmp_obj(dict[key1],dict[key2]);
    tmp_obj.assemble_matrix();
//    tmp_obj.print_matrix();

    int result = tmp_obj.get_last();
    return result;
}


int main()
{
    Map Sequences;
    str line, key, value;
    std::ifstream file("proteome.seq");
    
    while (true) 
    {
            while ( line[0] != '>')
            {
                std::getline(file,line);
                if ( file.eof() )
                    goto exit_of_nested_loop;
            }
       
        
        auto x_pos = remove_if(line.begin(), line.end(), isspace);
        line.erase(x_pos, line.end());
        key = line.substr(1);
        value.clear();

        while ( !line.empty() )
        {
            std::getline(file,line);
            auto x_pos = remove_if(line.begin(), line.end(), isspace);
            line.erase(x_pos, line.end());
            value += line;
        }   
        
        Sequences[key] = value;
    }

exit_of_nested_loop:
    file.close();

//    print_dict(Sequences);

    std::cout << Lev_Dist("bA16L21.2.1", "bA9F11.1", Sequences) << '\n';
}
