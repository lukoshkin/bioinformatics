#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <locale>
#include <algorithm>

using str = std::string;
using Map = std::map<str,str>;

class DM
{
    str sq1, sq2;
    int N, M;
    int ** matrix;

    public:
        DM (str,str);
        ~DM ();
        void assemble_matrix();
        int get_last();
        void print_matrix ();
};

DM::DM (str word1, str word2)
{
    sq1 = word1, sq2 = word2;
    N = sq1.size(), M = sq2.size(); 
    
    matrix = new int * [N+1];
    for (int i=0; i<N+1; i++)
        matrix[i] = new int [M+1];     
}

DM::~DM ()
{
    for (int i=0; i<N; i++)
        delete [] matrix[i];
    delete [] matrix;
}

void DM::assemble_matrix ()
{
    for (int i=0; i<N+1; i++)
        matrix[i][0] = i;

    for (int i=1; i<M+1; i++)
        matrix[0][i] = i;  

    for (int i=1; i<N+1; i++)
    {
        for (int j=1; j<M+1; j++)
        {
            int a = matrix[i][j-1] + 1;
            int b = matrix[i-1][j] + 1;
            int c = matrix[i-1][j-1] + ( sq1[i-1] != sq2[j-1] ); 
            matrix[i][j] = std::min(a,std::min(b,c));
        }
    }
}

void DM::print_matrix ()
{
    std::cout << " \t \t";
    for (int i=0; i<M; i++)
        std::cout << sq2[i] << '\t';
    std::cout << sq2[M] << '\n';

    std::cout << " \t";
    for (int i=0; i<M; i++)
        std::cout << matrix[0][i] << '\t';
    std::cout << matrix[0][M] << '\n';
     
    for (int i=1; i<N+1; i++)
    {
        std::cout << sq1[i-1] << '\t';
        for (int j=0; j<M; j++)
            std::cout << matrix[i][j] << '\t';
        std::cout << matrix[i][M] << '\n';
    }
}

int DM::get_last()
{
    return matrix[N][M];
}

void print_dict (Map dict)
{
    for (auto it : dict)
    {
            std::cout << it.first << " : " << it.second << '\n';
    }
}

int Lev_Dist(str key1, str key2, Map dict)
{
    DM tmp_obj(dict[key1],dict[key2]);
    tmp_obj.assemble_matrix();
//    tmp_obj.print_matrix();

    int result = tmp_obj.get_last();
    return result;
}


int main()
{
    Map Sequences;
    str line, key, value;
    std::ifstream file("proteome.seq");
    
    while (true) 
    {
            while ( line[0] != '>')
            {
                std::getline(file,line);
                if ( file.eof() )
                    goto exit_of_nested_loop;
            }
       
        
        auto x_pos = remove_if(line.begin(), line.end(), isspace);
        line.erase(x_pos, line.end());
        key = line.substr(1);
        value.clear();

        while ( !line.empty() )
        {
            std::getline(file,line);
            auto x_pos = remove_if(line.begin(), line.end(), isspace);
            line.erase(x_pos, line.end());
            value += line;
        }   
        
        Sequences[key] = value;
    }

exit_of_nested_loop:
    file.close();

//    print_dict(Sequences);

    std::cout << Lev_Dist("bA16L21.2.1", "bA9F11.1", Sequences) << '\n';
}
